FROM php:7.4.24-cli-alpine3.14

RUN apk add --no-cache --update autoconf file g++ gcc libzip libzip-dev libc-dev make pkgconf re2c libxml2-dev libpng-dev freetype-dev libjpeg-turbo-dev libpng-dev npm

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli 

RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql 

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-install gd && docker-php-ext-enable gd 

RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip

RUN apk del autoconf file g++ gcc libc-dev make pkgconf re2c libxml2-dev libzip libzip-dev

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer
	
COPY php.ini /usr/local/etc/php/conf.d/php.ini
	
COPY entrypoint /usr/local/bin/entrypoint
RUN chmod 777 /usr/local/bin/entrypoint
	
ENTRYPOINT ["/bin/sh", "-c"]